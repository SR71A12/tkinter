# Tkinter + Python: how to program simple user interfaces on X11+Python embedded environments


## Links

* [Tkinter: invoke event in main loop](https://stackoverflow.com/questions/270648/tkinter-invoke-event-in-main-loop)
* [Tkinter: How to use threads to preventing main event loop from “freezing”](https://stackoverflow.com/questions/16745507/tkinter-how-to-use-threads-to-preventing-main-event-loop-from-freezing)
* [How do you run your own code alongside Tkinter's event loop?](https://stackoverflow.com/questions/459083/how-do-you-run-your-own-code-alongside-tkinters-event-loop)
* [How to pass an argument to event handler in tkinter?](https://stackoverflow.com/questions/3296893/how-to-pass-an-argument-to-event-handler-in-tkinter)
* [Pass additional parameter through event_generate?](https://stackoverflow.com/questions/18804888/pass-additional-parameter-through-event-generate)
* [how to use Tcl/Tk bind function on Tkinter's widgets in Python?](https://stackoverflow.com/questions/41912004/how-to-use-tcl-tk-bind-function-on-tkinters-widgets-in-python)
* [Tkinter button expand using grid](https://stackoverflow.com/questions/53073534/tkinter-button-expand-using-grid)

### Author: Andrea Montefusco

Currently employed as network architect, always Internet working man, 
real C/C++ programmer in the past, network and Unix system engineer as needed, 
HAM Radio enthusiast (former IW0RDI, now IW0HDV), aeromodeller (a person who builds and flies model airplanes) since 1976 (ex FAI10655).

 * [Web](https://www.montefusco.com) 
 * [Github](https://github.com/amontefusco)
 