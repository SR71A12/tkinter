from Tkinter import  *
import subprocess
import tkFont
import threading
import subprocess
import shlex

root=Tk()
root.geometry("800x480")

class BistableButton(Button,object):

    photo_on=PhotoImage(file="on-100.png")
    photo_off=PhotoImage(file="off-100.png")

    def __init__(self, parent_w, *args, **kwargs):
        Button.__init__(self, parent_w, *args, **kwargs)
        self.parent_w = parent_w
        self.pressed = True
        self.set_image()
        self.config(command=self.callback)

    def set_image(self):
        if self.pressed == False:
            super(BistableButton,self).config(image = BistableButton.photo_on)
        else:
            super(BistableButton,self).config(image = BistableButton.photo_off)

    def callback(self):
        if self.pressed == False:
            self.pressed = True
        else:
            self.pressed = False

        self.set_image()
        self.on_change(self.pressed)

    def on_change (self, new_status):
	pass


class SCSBistableButton(BistableButton,object):
    mqtt_srv = ""

    @staticmethod
    def set_mqtt_server(x):
        SCSBistableButton.mqtt_srv = x

    @staticmethod
    def get_mqtt_server():
        return SCSBistableButton.mqtt_srv

    def __init__(self, cmd, parent_w, *args, **kwargs):
        BistableButton.__init__(self, parent_w, *args, **kwargs)
        self.scs = cmd

    def on_change (self, new_status):
		print self.scs + " " + str(new_status)

		if new_status == True:
			cmd = 1
		else:
			cmd = 0

		xcmd = "mosquitto_pub -h " + SCSBistableButton.mqtt_srv + " -t " + "'" + self.scs + "/command'" + " -m '"+ str(cmd) +"' -r"

		subprocess.call(xcmd, shell=True)

    def force_status (self, new_status):
		print "Force Status %s" % new_status
		if new_status == "1":
			self.pressed = True
		else:
			self.pressed = False
		self.set_image()


BUTTONS = [

  [
    ['Cucina soffitto', "scs/0/8/4"],
    ['Cucina parete',   "scs/0/5/8"],
  ],

  [
    ['Cucina stanzino', "scs/0/5/9"],
    ['Cucina esterno',  "scs/0/2/1"],
  ],

]

#
# main
#
mqtt_server = "localhost"

if len(sys.argv) >= 2:
	mqtt_server = sys.argv[1]

SCSBistableButton.set_mqtt_server (mqtt_server)

helv36 = tkFont.Font(family='Helvetica', size=36, weight='bold')
helv28 = tkFont.Font(family='Helvetica', size=28, weight='bold')
helv24 = tkFont.Font(family='Helvetica', size=24, weight='bold')
helv18 = tkFont.Font(family='Helvetica', size=18, weight='bold')

root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0, weight=1)

grid_frame = Frame(root,padx=5,pady=5,bg="red")
grid_frame.grid(row=0,column=0, sticky=NSEW)

for row, row_buttons in enumerate(BUTTONS):
    grid_frame.grid_rowconfigure(row, weight=1)

    for col, data in enumerate(row_buttons):
        grid_frame.grid_columnconfigure(col, weight=1)
        b = SCSBistableButton(data[1], grid_frame, text=data[0], bg="white", compound=TOP, fg="black", font=helv28, background="#EEEEEE")
        b.grid(row=row, column=col, sticky='EWNS')
        # append the widget
        wx = b
        data.append (wx)




#
#
#
def event_thread_cb_2(x):
    print "Event from background thread [%s]" % x.rstrip()

    # remove the trailing "status"
    ll = x.split()
    # isolate the new status value
    cx = ll[0].rpartition("/")[0]
    # search in the buttons list
    for row, row_buttons in enumerate(BUTTONS):
       for col, data in enumerate(row_buttons):
    	   if data[1] == cx:
              data[2].force_status (ll[1])



class ThreadedTask(threading.Thread):
    def __init__(self, root):
        threading.Thread.__init__(self)
        self.root = root

    def run(self):
        while True:
           process = subprocess.Popen (shlex.split("mosquitto_sub -h " + SCSBistableButton.get_mqtt_server() + " -t 'scs/+/+/+/status' -v"), stdout=subprocess.PIPE)
           while True:
			   output = process.stdout.readline()
			   ll = output.split()
			   self.root.event_generate ("<<EventThread>>", when="tail", data=output)


cmd = root.register(event_thread_cb_2)
root.tk.call ("bind", root, "<<EventThread>>", cmd + " %d")

# start the background thread monitoring the MQTT status meesages
tt = ThreadedTask(root)
tt.start()

# loop of GUIevents
root.mainloop()

