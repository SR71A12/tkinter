from Tkinter import *

master = Tk()
#master.minsize(300,100)
master.geometry("800x480")


class BistableButton(Button,object):

    photo_on=PhotoImage(file="on-100.png")
    photo_off=PhotoImage(file="off-100.png")

    def __init__(self, parent_w, *args, **kwargs):
        Button.__init__(self, parent_w, *args, **kwargs)
        self.parent_w = parent_w
        self.pressed = False
        self.set_image()
        self.config(command=self.callback)
        print "cccccc"

    def set_image(self):
        print self
        print self.parent_w
        if self.pressed == False:
            super(BistableButton,self).config(image = BistableButton.photo_on)
        else:
            super(BistableButton,self).config(image = BistableButton.photo_off)

    def callback(self):
        print "click callback"
        if self.pressed == False:
            self.pressed = True
        else:
			self.pressed = False

        self.set_image()
        self.on_change(self.pressed)

    def on_change (self, new_status):
		pass

class SCSBistableButton(BistableButton,object):
    def __init__(self, cmd, parent_w, *args, **kwargs):
        BistableButton.__init__(self, parent_w, *args, **kwargs)
        self.scs = cmd

    def on_change (self, new_status):
		print self.scs + " " + str(new_status)




b1 = BistableButton(master)
b1.pack()

b2 = SCSBistableButton("/scs/0/1/2",master)
b2.pack()

b3 = SCSBistableButton("/scs/0/1/3",master)
b3.pack()

mainloop()


