import Tkinter as tk
import threading
import time

def write_slogan():
    print("Tkinter is easy to use!")
    root.event_generate ("<<Foo>>", when="tail")


def event_cb(x):
	print "EEEEEEEEEE!!!!" + str(x)
	


root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

root.bind("<<Foo>>", event_cb)

button = tk.Button(frame, 
                   text="QUIT", 
                   fg="red",
                   command=quit)
button.pack(side=tk.RIGHT)

slogan = tk.Button(frame,
                   text="Hello",
                   command=write_slogan)
slogan.pack(side=tk.LEFT)


c = 0

def event_thread_cb(x):
	global c
	print "EEEEEEEEEE from thread !!!!" + str(x)
	if c % 2 == 0:
		slogan.config(fg="red")
	else:
		slogan.config(fg="yellow")
	c=c+1
	

class ThreadedTask(threading.Thread):
    def __init__(self, root):
        threading.Thread.__init__(self)
        self.root = root

    def run(self):
        while True:
           print "TTT 1"
           time.sleep(1)
           self.root.event_generate ("<<EventThread>>", when="tail")
           #self.root.bind("<<Foo>>", event_cb)
           print "TTT 2"
      
        

root.bind("<<EventThread>>", event_thread_cb)


# start a thread
tt = ThreadedTask(root)
tt.start()

root.mainloop()

