from Tkinter import  *
import subprocess

def shut():
    subprocess.call("systemctl halt", shell=True)  

root=Tk()
root.geometry("800x480")
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0, weight=1)


def exit():
    root.destroy()

def scs_on():
    subprocess.call("mosquitto_pub -h 192.168.0.55 -t 'scs/0/8/4/command' -m '0'", shell=True)




keyboard=Frame(root,padx=5,pady=5,bg="red")
keyboard.grid(row=0,column=0, sticky=NSEW)

for row, row_buttons in enumerate([['Cucina soffitto', 'b', 'b', 'd'], ["1", "2", "EXIT", "HALT"]]):
    keyboard.grid_rowconfigure(row, weight=1)
    for col, text in enumerate(row_buttons):
        keyboard.grid_columnconfigure(col, weight=1)
        b = Button(keyboard, padx=5, pady=5, text=text)
        b.grid(row=row, column=col, sticky='EWNS')
        if text == "HALT":
            b.config(command=shut)
        if text == "EXIT":
            b.config(command=exit)

        if text == "Cucina soffitto":
            b.config(command=scs_on)



root.mainloop()

