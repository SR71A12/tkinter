import Tkinter as tk

counter = 30 

def counter_label(label):
  global counter 
  counter = 0

  def count():
    global counter
    counter += 1
    label.config(text=str(counter))
    label.after(1000, count)

  count()
 
 
root = tk.Tk()
root.geometry("480x320") #You want the size of the app to be 500x500
root.resizable(0, 0) #Don't allow resizing in the x or y direction

root.title("Counting Seconds")

label = tk.Label(root, fg="dark green")
label.pack()
counter_label(label)

button = tk.Button(root, text='Stop', width=25, command=root.destroy)
button.pack()

root.mainloop()
